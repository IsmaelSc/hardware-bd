
const cant_reg_pag=10;
var user = new Array(0,0);

var get = getUrlVars();
var pag_actual = get['pag'];

function getUrlVars() { //funcion para extraer por url un get
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

$(document).ready(function(){
	$("#salir").click(function(){
		$.ajax({
			url: "php/tablas.php",
			data: "salir=1",
			type: "POST",
			dataType: "json",
			success:function(s){
				window.location="index.html";
			}
		});
	});
	// paginador('1',"$_GET['pag']");
	// console.log(paginador);
	
	
	crearTabla();
		

	$.ajax({
		url: "php/tablas.php",
		type: "POST",
		dataType: "json",
		success:function(s){
			$.ajax({
				url: "php/listado_usuarios.php",
				type: "POST",
				dataType: "json",
				success:function(s){
					for (var i = 0; i < s.length; i++){
						$("#aqui").html($("#aqui").html() + "<label class='box' ><input type='checkbox' class='aqui_circulo' onclick=nombre(" + s[i]['id'] +")>" + s[i]['nombre'] + " " + s[i]['apellido'] + "</input><span class='checkmark'></span></label><br>");

					}

					
					$('label.input.checkmark').on('change', function(evt) {
   						if($(this).siblings(':checked').length >= 1) {
       					this.checked = false;
  						 }
					});
				}
			});
		}
	});

});

function crearTabla () {
	$.ajax({
		url: "php/tablas.php",
		data: {'pag_actual': pag_actual,'mostrar': cant_reg_pag},
		type: "POST",
		dataType: "json",
		success:function(s){
			table= s['total_registros'].length;

				if(s['error']){
					$("#body").html(s['error']);
				}
				else{
					var tabla="<table id='tabla' style='background-color: mediumSeaGreen; margin-bottom: 5%' border='1'><tr><th>ID</th><th>Error</th><th>Computadora</th>"
						if(s['rol']==1){
							tabla+="<th>Tecnico</th>";
						}
						tabla+="<th>Fecha de alta</th><th>Acciones</th></tr>";
						for (var reg in s['tabla']){
							tabla+="<tr>";
							var cont=0;
							for (var valor in s['tabla'][reg]){
								cont++;
								if(!s['tabla'][reg][valor]){ //si no hay nada hace una columna vacia
									tabla+="<td></td>";
								}
								else{
									if(cont==2){
										tabla+="<td align='left'>";
									}
									else{
										tabla+="<td>";
									}
									tabla+=s['tabla'][reg][valor]+"</td>";
								}
							}
							if(s['rol']==1){
								//tabla+="<button onclick='guardar("+s['tabla'][reg]['id']+")' type='button' class='btn btn-info btn-lg' data-toggle='modal' data-target='#myModal'>Terminar</button></td>";
								tabla+="<td><button style='background-color: mediumSeaGreen; border: none' onclick='registro("+s['tabla'][reg]['id']+")' type='button' class='btn btn-info btn-lg btn-asignar' data-toggle='modal' data-target='#myModal'>Asignar</button></td>";

							}
							else if (s['rol']==2){

								tabla+="<td><button id='terminar' style='background-color: mediumSeaGreen; border: none' class='btn btn-info btn-lg btn-asignar' onclick='terminar("+s['tabla'][reg]['id']+")'>Terminado</button></td>"; //termianr es una funcion que le mada el id  que hace un update

							}
							tabla+="</tr>";

					}
				tabla+="</tabla>";
				var pagi= paginador(get['pag'],table,15);
				$("#tabla").html(tabla);
				$("#paginador").html(pagi);


					if(s['rol']==1){
						$("#lista_tabla").html("<a style='text-decoration: none; cursor: default' onclick=window.location.href='listado_pc.html'>Listado de Computadoras</a>");
					}
					if(s['rol']==2){
						$("#lista_tabla").html("<a style='text-decoration: none; cursor: default' onclick=window.location.href='listado_pc.html'>Pc´s reparadas</a>");
					}

				}
			}
		
		});
}

function asignar(){
	$.ajax({
		url: "php/asignar.php",
		data: "id="+user[0]+'&usuario='+user[1],
		type: "POST",
		dataType: "json",
		success:function(s){
			$("#error_asignar").html(s['error']);
			crearTabla();
		}
	});

	
}

function terminar(id){
	var resultado_terminar;
	var terminado= prompt("Arreglo realizado:");
	if (terminado==""||terminado==null) {
		resultado_terminar = "El campo se a completado vacío";
    } 
    else {
    	var resultado_terminar="Se a enviado correctamente ;)";

    	$.ajax({
			url: "php/terminar.php",
			data: "id="+id+'&solucion='+terminado,
			type: "POST",
			dataType: "json",
			success:function(s){
				$("#error_terminado").html(s["error"]);
				crearTabla();
			}
		});
    }
    //document.getElementById("error_terminado").innerHTML= resultado_terminar;	
    $("#error_terminado").html(resultado_terminar);
    
}

function registro(num){
	user[0] = num;
}
function nombre(num){
	user[1] = num;
}

