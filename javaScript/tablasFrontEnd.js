$(document).ready(function(){
	$("#signOculto1").hover(function(){
		if( $("#info1").hasClass("oculto") ){
			$("#info1").removeClass("oculto");
			$("#info1").addClass("visible");
		}
		else{
			$("#info1").removeClass("visible");
			$("#info1").addClass("oculto");
		}
	});

	$("#signOculto2").hover(function(){
		if($("#info2").hasClass("oculto")){
			$("#info2").removeClass("oculto");
			$("#info2").addClass("visible");
		}
		else{
			$("#info2").removeClass("visible");
			$("#info2").addClass("oculto");
		}
	});

	$(function(){
			$("#contBarra").load("barra.html"); 
	});
	
	$("#btnDisplay").click(function(){
		if($("#mySidebar").hasClass("none")){
			$("#mySidebar").removeClass("none");
			$("#mySidebar").addClass("block");

		}
		else{
			$("#mySidebar").removeClass("block");
			$("#mySidebar").addClass("none");
		}
	});
	
	$("#btnDisplay").click(function(){
		if($("#flecha").hasClass("fas fa-angle-left")){
			$("#flecha").removeClass("fas fa-angle-left");
			$("#flecha").addClass("fas fa-angle-right");

		}
		else{
			$("#flecha").removeClass("fas fa-angle-right");
			$("#flecha").addClass("fas fa-angle-left");
		}
	});
});
