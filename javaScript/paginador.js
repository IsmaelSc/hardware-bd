function paginador(mostrar, table, pags_x_pag){

		cant_pags= table/cant_reg_pag;
		cant_total =Math.ceil(cant_pags);

		var paginador = "";
		var get = getUrlVars();
		
		if(get['pag']>1){
			if(get['pag']>Math.ceil(pags_x_pag/2)){
				paginador += "<a onclick='paginador(1,10)' href='?pag=1'><i class='fas fa-grip-lines-vertical'></i><i class='fas fa-arrow-circle-left'></i></a>";
			}
			paginador += "<a onclick='paginador("+(get['pag']-1)+",10)' href='?pag="+(get['pag']-1)+"'> <i class='fas fa-arrow-left'></i> </a>";
		}
		var inicio;
		if(get['pag']<=Math.ceil(pags_x_pag/2)){
			inicio = 1;
		}
		else if(get['pag']>cant_total-Math.ceil(pags_x_pag/2)){
			inicio = cant_total-pags_x_pag;
			pags_x_pag++;
		}
		else{
			inicio = get['pag']-Math.ceil(pags_x_pag/2)+1;
			pags_x_pag++;
		}


		for (var i=inicio,vueltas=0;vueltas<pags_x_pag ; i++,vueltas++) {
			if(i == get['pag'])
				paginador+="<a class='paginas colorNuevo' onclick='paginador("+i+",10)' href='?pag="+i+"'>"+i+" </a>";			
			else
				paginador+="<a class='paginas colorViejo' onclick='paginador("+i+",10)' href='?pag="+i+"'>"+i+" </a>";
		}
		if(get['pag']<cant_total){
			paginador += "<a onclick='paginador("+(parseInt(get['pag'])+1)+",10)' href='?pag="+(parseInt(get['pag'])+1)+"'> <i class='fas fa-arrow-right'></i></i></a>";
			if(!(get['pag']>cant_total-Math.ceil(pags_x_pag/2)-1)){
				paginador += "<a onclick='paginador("+cant_total+",10)' href='?pag="+cant_total+"'> <i class='fas fa-arrow-circle-right'></i><i class='fas fa-grip-lines-vertical'></i></a>";
			}
		}
		pag_actu= get['pag'];
		paginador +=  '<br><br>'+ pag_actu + " de "+ cant_total;
		return paginador;
}

function getUrlVars() { //funcion para extraer por url un get
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}