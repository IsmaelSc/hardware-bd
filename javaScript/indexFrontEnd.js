$(document).ready(function(){
	$.ajax({
		url: "php/tablas.php",
		data: "salir=1",
		type: "POST",
		dataType: "json",
		success:function(s){
			
		}
	});

	$("#signOculto1").hover(function(){
		if( $("#info1").hasClass("oculto") ){
			$("#info1").removeClass("oculto");
			$("#info1").addClass("visible");
		}
		else{
			$("#info1").removeClass("visible");
			$("#info1").addClass("oculto");
		}
	});

	$("#signOculto2").hover(function(){
		if($("#info2").hasClass("oculto")){
			$("#info2").removeClass("oculto");
			$("#info2").addClass("visible");
		}
		else{
			$("#info2").removeClass("visible");
			$("#info2").addClass("oculto");
		}
	});

	$(document).ready(function(){
			$("#contBarra").load("barra.html"); 
	});
	
	$("#btnDisplay").click(function(){
		if($("#mySidebar").hasClass("none")){
			$("#mySidebar").removeClass("none");
			$("#mySidebar").addClass("block");

		}
		else{
			$("#mySidebar").removeClass("block");
			$("#mySidebar").addClass("none");
		}
	});
	
	$("#btnDisplay").click(function(){
		if($("#flecha").hasClass("fas fa-angle-left")){
			$("#flecha").removeClass("fas fa-angle-left");
			$("#flecha").addClass("fas fa-angle-right");

		}
		else{
			$("#flecha").removeClass("fas fa-angle-right");
			$("#flecha").addClass("fas fa-angle-left");
		}
	});
});
		
function saltar(e,id){
	// Obtenemos la tecla pulsada
	(e.keyCode)?k=e.keyCode:k=e.which;
	// Si la tecla pulsada es enter (codigo ascii 13)
	if(k==13)
	{
		// Si la variable id contiene "submit" enviamos el formulario
		if(id=="submit")
		{
			document.forms[0].submit();
		}else{
			// nos posicionamos en el siguiente input
			document.getElementById(id).focus();
		}
	}
}