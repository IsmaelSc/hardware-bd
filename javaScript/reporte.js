$(document).ready(function(){
	$("#submit").click(function(){
		$.ajax({
			url: "php/reporte.php",
			data: "pc="+ $("#input_equipo").val() + "&error="+ $("#input_problema").val(),
			type: "POST",
			dataType: "json",
			success:function(s){
				$("#reporte").html( s['reporte'] );
			}
		});
		$("#input_problema").val("");
		$("#input_equipo").val("");
	});
});