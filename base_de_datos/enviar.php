<?php
	$link = mysqli_connect('localhost','root','');
	
	if (!$link){
		die(mysqli_connect_error);
	}
	
	mysqli_query($link,'CREATE DATABASE hardware_bd');
	
	$seleccion = mysqli_select_db ($link,'hardware_bd');
	
	if(!$seleccion){
		die(mysqli_error($link));
	}
	
	function enviarSQL($link,$query){
		$resultado=mysqli_query($link,$query);
		if(!$resultado){
			die(mysqli_error($link));
		}
		if(!($resultado===true)){
			$resultado = mysqli_fetch_all($resultado,MYSQLI_ASSOC);
			return $resultado;
		}
		else return true;
	}
	
?>